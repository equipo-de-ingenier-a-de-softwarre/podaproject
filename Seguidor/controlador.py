from modelos import Solicitud, Comentario
from pymongo import MongoClient
from datetime import datetime

def connect2collection():
    cliente = MongoClient("mongodb+srv://admin:admin@cluster0.yxk9x.mongodb.net/myFirstDatabase?retryWrites=true&w=majority")
    print(cliente.list_database_names())
    db = cliente.Solicitud.Solicitud
    return(db)

def generarSolicitud(folio, responsable, estatus, fechaSol, fechaLim):
    #Generamos la solicitud
    tmp = Solicitud()
    tmp.folio = folio
    tmp.responsable = responsable
    tmp.estatus = estatus
    tmp.fechaSolicitud = fechaSol
    tmp.fechaLimite = fechaLim
    return(tmp)

def agregarComentario(solicitud, titulo, contenido, url):
    # Obtenemos el tiempo de la solicitud
    date = datetime.now()
    
    # Generamos el comentario
    tmp = Comentario()
    tmp.fecha = date.strftime('%Y/%m/%d')
    tmp.hora = date.strftime('%H:%M')
    tmp.titulo = titulo
    tmp.contenido = contenido
    tmp.fotoURL = url
    
    # Agregamos el comentario a la solicitud
    solicitud.addComentario(tmp)

def writeSolicitud(db, solicitud):
    x = db.insert_one(solicitud.toDoc())
    print(x.inserted_id);
    return x.inserted_id

def readSolicitud(db, folio):
    # Se hace la solicitud a la base de datos
    resp = db.find_one({"folio": folio})
    
    if resp:
        tmp = generarSolicitud(folio, resp["responsable"], resp["estatus"], resp["fechaSolicitud"], resp["fechaLimite"])
        for comment in resp["comentarios"]:
            cmt = Comentario()
            cmt.fecha = comment["fecha"]
            cmt.hora = comment["hora"]
            cmt.titulo = comment["titulo"]
            cmt.contenido = comment["contenido"]
            cmt.fotoURL = comment["fotoURL"]
            tmp.addComentario(cmt)
    else:
        tmp=Solicitud()
    return(tmp)



