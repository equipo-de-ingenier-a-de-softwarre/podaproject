class Solicitud:
    folio = "0"
    estatus = "Turnado"
    responsable = ""
    fechaSolicitud = ""
    fechaLimite = ""
    comentarios=list()
    
    def addComentario(self, comentario):
        self.comentarios.append(comentario.toDoc())
    
    def toDoc(self):
        doc = {
                'folio': self.folio,
                'estatus': self.estatus,
                'responsable': self.responsable,
                'fechaSolicitud': self.fechaSolicitud,
                'fechaLimite': self.fechaLimite,
                'comentarios': self.comentarios
                }
        return(doc)
    
class Comentario:
   fecha = ""
   hora = ""
   titulo = ""
   contenido = ""
   fotoURL = ""

   def toDoc(self):
       doc = {
               'fecha': self.fecha,
               'hora': self.hora,
               'titulo': self.titulo,
               'contenido': self.contenido,
               'fotoURL': self.fotoURL
               }
       return(doc)

