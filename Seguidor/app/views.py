# views.py
import sys
from flask import render_template,make_response, request
from app import app
from controlador import connect2collection, readSolicitud


# Se genera la conexión con la base de datos
db=connect2collection()


@app.route('/')
def login():
   resp=make_response(render_template("login.html")) 
   return resp

@app.route('/seguidor')
def indexUser():
   resp=make_response(render_template("FolioEntryUser.html"))
   return resp

@app.route('/index')
def index():
   resp=make_response(render_template("FolioEntry.html"))
   #resp.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
   #resp.headers["Pragma"] = "no-cache"
   #resp.headers["Expires"] = "0"
   return resp

@app.route('/editor', methods = ['POST']) 
def editor():  
   if request.method == 'POST':
      formResponse = request.form
      print(formResponse)
      value = formResponse.items()
      for element in value:
         print(element) 
      resp=make_response(render_template("editor.html", solicitud=readSolicitud(db, formResponse['folio']) ))
      return resp
   return render_template("error.html") 

@app.route('/peticion', methods = ['POST']) 
def visor():  
   if request.method == 'POST':
      formResponse = request.form
      print("Mirame!!")
      for i in formResponse:
          print("Hagale: ")
          print(i)
      value = formResponse.items()
      for element in value:
         print(element) 
      resp=make_response(render_template("visor.html", solicitud=readSolicitud(db, 'SUAC-310321763015') ))
      return resp
   return render_template("error.html") 

@app.errorhandler(500)
def internal_error(error):
   return render_template("error.html") 
