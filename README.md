# Sistema de petición de servicios públicos
## Poda y tala de árboles en la CDMX

## Seguidor de peticiónes

#### Dependencias

- LINUX (teorícamente cualquiér sistema POSIX compatible)
- Python3
- Flask

#### Ejecución

Para ejecutar el panel de actualización se debe iniciar el entorno virtual de python

` python3 -m venv Seguidor_Panel`

Posteriormente se configura Flask para ejecutar la aplicación

`export FLASK_APP=app`

Si se ejecuta como entorno de pruebas debe configurarse como

`export FLASK_ENV=development`

Finalmente se ejecuta el servidor con

`flask run`


