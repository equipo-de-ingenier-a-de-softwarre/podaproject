<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Formato</title>
  <link rel="stylesheet" href="css/bootstrap.min.css" />
  <link rel="stylesheet" href="css/main.css" />
  <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
</head>

<body>
  <div class="container mb-4">
    <img src="images/logo-cdmx.png" class="img-fluid" />
    <div class="container rounded fondo-container border border-dark">
      <h1 class="text-center text-secondary p-4">Formato único de registro</h1>
      <form>
        <div class="row mb-3">
          <label for="nombre-tramite" class="col-sm-6 col-form-label">NOMBRE DE TRÁMITE/SERVICIO O ACTIVIDAD DE SIMILAR
            NATURALEZA:</label>
          <div class="col-sm-6">
            <input type="text" class="form-control" id="nombre-tramite" placeholder="Poda y derribo de árboles y ramas en vía pública" />
          </div>
        </div>
        <div class="row mb-3">
          <label for="fecha" class="col-sm-4 col-form-label">Ciudad de México, a</label>
          <div class="col-sm-4">
            <input type="date" class="form-control" id="fecha" />
          </div>
        </div>
        <div class="row mb-3">
          <label for="encargado" class="col-sm-4 col-form-label">Secretario, Alcalde, Director General:
          </label>
          <div class="col-sm-4">
            <select class="form-select" aria-label="Default select">
              <option selected>Seleccione una opción</option>
              <option value="1">Secretario</option>
              <option value="2">Alcalde</option>
              <option value="3">Director General</option>
            </select>
          </div>
        </div>
        <div>
          <p class="small" style="text-align: justify">
            Presente Declaro bajo protesta de decir verdad que la información
            y documentación proporcionada es verídica, por lo que en caso de
            existir falsedad en ella, tengo pleno conocimiento que se
            aplicarán las sanciones administrativas y penas establecidas en
            los ordenamientos respectivos para quienes se conducen con
            falsedad ante la autoridad competente, en términos del artículo 32
            de la Ley de Procedimiento Administrativo de la Ciudad de México y
            con relación al 311 de Código Penal para el Distrito Federal.
          </p>
        </div>
        <div class="mb-3">
          <label for="exampleFormControlTextarea1" class="form-label display-6">
            <p class="text-center rounded label">
              Información al interesado sobre el tratamiento de sus datos
              personales.
            </p>
          </label>
          <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
        </div>
        <div class="form-check">
          <p>Seleccione una opción:</p>
          <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault1" />
          <label class="form-check-label" for="flexRadioDefault1">
            Persona Física
          </label>
        </div>
        <div class="form-check">
          <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault2" checked />
          <label class="form-check-label mb-4" for="flexRadioDefault2">
            Persona Moral
          </label>
        </div>
        <div>
          <p class="display-6 text-center rounded label">
            Domicilio para oir y recibir notificaciones y documentos en la
            Ciudad de México.
          </p>
        </div>
        <div>
          <p class="small text-danger">*Los datos de este bloque son obligatorios.</p>
        </div>
        <div class="row mb-3">
          <label for="calle" class="col-sm-2 col-form-label">Calle:</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" id="calle">
          </div>
        </div>
        <div class="row mb-3">
          <label for="no-ext" class="col-sm-2 col-form-label">No. Exterior:</label>
          <div class="col-sm-1">
            <input type="text" class="form-control" id="no-ext">
          </div>
          <label for="no-int" class="col-sm-2 col-form-label">No. Interior:</label>
          <div class="col-sm-1">
            <input type="text" class="form-control" id="no-int">
          </div>
          <label for="colonia" class="col-sm-2 col-form-label">Colonia:</label>
          <div class="col-sm-4">
            <input type="text" class="form-control" id="colonia">
          </div>
        </div>
        <div class="row mb-3">
          <label for="alcaldia" class="col-sm-2 col-form-label">Alcaldía:</label>
          <div class="col-sm-4">
            <select class="form-select" aria-label="Default select">
              <option selected>Seleccione una alcaldía</option>
              <option value="1">Magdalena Contreras</option>
              <option value="2">Gustavo A. Madero</option>
              <option value="3">Venustiano Carranza</option>
            </select>
          </div>
          <label for="cp" class="col-sm-2 col-form-label">C.P.:</label>
          <div class="col-sm-4">
            <input type="text" class="form-control" id="cp">
          </div>
        </div>
        <div class="row mb-3">
          <label for="correo" class="col-sm-4 col-form-label">Correo electrónico para recibir notificaciones:</label>
          <div class="col-sm-8">
            <input type="email" class="form-control" id="correo">
          </div>
        </div>
        <div>
          <p class="display-6 text-center rounded label">
            Persona autorizada para oír y recibir notificaciones y documentos.
          </p>
        </div>
        <div class="row mb-3">
          <label for="nombre-res" class="col-sm-2 col-form-label">Nombre (s):</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" id="nombre-res ">
          </div>
        </div>
        <div class="row mb-3">
          <label for="apaterno" class="col-sm-2 col-form-label">Apellido paterno:</label>
          <div class="col-sm-4">
            <input type="text" class="form-control" id="apaterno">
          </div>
          <label for="amaterno" class="col-sm-2 col-form-label">Apellido materno:</label>
          <div class="col-sm-4">
            <input type="text" class="form-control" id="amaterno">
          </div>
        </div>
        <div>
          <p class="display-6 text-center rounded label">
            Requisitos.
          </p>
        </div>
        <div class="row mb-3">
          <label for="formFile" class="col-sm-3 col-form-label">Documento de identificación oficial:</label>
          <div class="col-sm-9">
            <select class="form-select" aria-label="Default select">
              <option selected>Seleccione un documento de identificación oficial</option>
              <option value="1">Cartilla del Servicio Militar Nacional</option>
              <option value="2">Cédula Profesional</option>
              <option value="3">Credencial para Votar</option>
              <option value="4">Pasaporte</option>
            </select>
          </div>
        </div>
        <div class="row mb-3">
          <label for="formFile" class="col-sm-3 col-form-label">Adjuntar archivo .pdf o imágenes</label>
          <div class="col-sm-9">
            <input class="form-control" type="file" id="formFile">
          </div>
        </div>
        <div class="row mb-3">
          <label for="numero" class="col-sm-3 col-form-label">Número telefónico:</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="numero">
          </div>
        </div>
        <div class="mb-3">
          <label for="descripcion" class="form-label">Describir de manera clara y sucinta el servicio que requiere,
            así como el acto o hechos en que se apoye su petición.</label>
          <textarea class="form-control" id="descripcion" rows="3"></textarea>
        </div>
        <div class="mb-3">
          <label for="descripcion" class="form-label">Domicilio o ubicación exacta del lugar en donde se requiere el servicio (entre que calles se encuentra).</label>
        </div>

        <div class="mb-3 form-group input-group">
          <input type="text" id="search_location" class="form-control" placeholder="Buscar localización">
          <div class="input-group-btn">
            <button class="btn btn-primary get_map" type="submit">
              Buscar
            </button>
          </div>
        </div>
        <div id="geomap"></div>

        <h4>Detalles de la dirección</h4>
        <div class="row mb-3">
          <label for="direccion-arbol" class="col-sm-2 col-form-label ">Dirección</label>
          <div class="col-sm-4">
            <input type="text" class="form-control search_addr" id="direccion-arbol">
          </div>
          <label for="latitud" class="col-sm-1 col-form-label">Latitud:</label>
          <div class="col-sm-2">
            <input type="text" class="form-control search_latitude" id="latitud">
          </div>
          <label for="longitud" class="col-sm-1 col-form-label">Longitud:</label>
          <div class="col-sm-2">
            <input type="text" class="form-control search_longitude" id="longitud">
          </div>
        </div>
        <div>
          <p class="display-6 text-center rounded label">
            Observaciones.
          </p>
        </div>
        <div class="mb-3">
          <textarea class="form-control" id="descripcion" rows="3"></textarea>
        </div>











      </form>
      <div class="mb-3">
        <a href="crearPDF.php" class="btn btn-success">Crear PDF</a>
      </div>
    </div>
  </div>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery-3.6.0.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
  <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCM3fLOw5qH9UENFh6fET8TOgoI8eCQvjw&callback=initMap">
  </script>
  <script src="js/maps.js"></script>
</body>

</html>